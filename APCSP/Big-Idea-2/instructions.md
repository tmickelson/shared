Assembly language is binary code written in a form that humans can read. The Assembler takes assembly code and translates it line by line to the corresponding bit code.
