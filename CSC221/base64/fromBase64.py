def getbits(byte, num, pos):
    """
    Get the num bits beginning at position pos (counting from the lowest order
    bit 1 at the right) from byte, shifted into position as the rightmost num
    bits of a new byte padded to the left with 0s.
    
      >>> getbits(bytes([255])[0], 3, 5)
      7
      >>> getbits(bytes([42])[0], 3, 6)
      5
      >>> getbits(bytes([0b01101000])[0], 4, 7)
      13
      >>> bin(getbits(bytes([0b01101110])[0], 6, 7))
      '0b110111'
    """
    return (byte >> (pos - num)) & ~(255 << num)



def base64_decode(encoded):
    """
    >>> base64_decode("YWJj")
    'abc'
    >>> base64_decode("MTIz")
    '123'
    """
    base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    decoded_bytes = []

    # Remove padding characters '='
    encoded = encoded.rstrip('=')

    decoded = 0
    bit_position = 0

    for char in encoded:
        value = base64_chars.index(char)
        decoded = (decoded << 6) | value
        bit_position += 6

        while bit_position >= 8:
            bit_position -= 8
            decoded_bytes.append((decoded >> bit_position) & 0xFF)

    final = bytes(decoded_bytes).decode('utf-8')
    return final

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
