# Bias in Computer Innovation

## What is Bias?

Bias is a type of error that can create “unfair” outcomes by doing things like privileging one category over another, even if the bias isn’t an intended function of the algorithm.
 
Algorithmic Bias is a type of bias where a platform makes use of an algorithm to show users more things similar to things they've liked before. If someone, for example, likes a YouTube video of a cat jumping off a shelf, the user will probably be recommended more videos of cats jumping off of things. If someone likes a tweet from Greta Thunberg, they'll probably get more tweets about the environment. The algorithm shows the user more of what they know and like, and therefore cuts them off from other opinions, viewpoints, or even whole topics.

There are many examples of bias everywhere. For example, Facebook conducted a study in 2010 that showed that people were more likely to vote if a friend posted about voting. People were also more likely to vote the same way as their friend in that case.

## How can I recognize Algorithmic Bias?

For starters, recognize that bias is probably everywhere. If you're on social media, like Twitter, TikTok, YouTube, Instagram, or others, you're getting a biased worldview, whether you know it or not. Since you're human, (hopefully), you can probably understand that. For a computer, it might not.

If you're only getting one-sided opinions on things (politics or otherwise), stop and think for a second. Maybe your feed is biased (it is), and maybe there's another way to look at it (there is). 

Bias shows up in every platform that has an algorithm to show the user more of what they like. It's hard to avoid. So is there even a way to avoid it?

![](https://cdn.ttgtmedia.com/rms/onlineimages/enterprise_ai-bias_in_ml_modeling_route-f.png)

## Are there ways to avoid Bias?

Some methods of avoiding algorithmic bias include:
- Using a varied dataset
- Monitor the algorithm
- Being transparent about the algorithm

The first step for avoiding bias is using a voried and unbiased dataset. If the algorithm is going to be trained on biased data, there is no way that it is going to be unbiased.

The second step is monitoring the algorithm. Someone has to make sure that the algorithm does not change. Even if the algorithm does not change, the world can. Some things that were considered normal 20 years ago are not normal now.

The third step is being transparent. Allow others to see the goal of the algorithm. Hold yourself accountable for problems with the algorithm, and try to fix problems with it quickly.

