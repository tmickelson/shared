#!/usr/bin/python3

import sys

try:
    f = open(sys.argv[1], 'r')
except IndexError:
    print(f"{sys.argv[0]}: no file specified")
    quit()
except FileNotFoundError:
    print(f"{sys.argv[0]}: file \"{sys.argv[1]}\" not found")
    quit()

breeds = [line[:-1].split() for line in f.readlines()]

for line in range(len(breeds)):
    try:
        breeds[line][1] = int(breeds[line][1])
        breeds[line][2] = int(breeds[line][2])
    except ValueError:
        print(f"{sys.argv[0]}: {sys.argv[1]}: error on line {item - 1}")


def print_breeds():
    lengths = [0, 0, 0, 0]
    for line in range(len(breeds)):
        for item in range(len(breeds[line])):
            if len(str(breeds[line][item])) > lengths[item]:
                lengths[item] = len(str(breeds[line][item]))

    print("Info on breeds:")
    for line in breeds:
        for item in range(len(line)):
            print(str(line[item]) + ' ' * (lengths[item] - len(str(line[item]))), end=' ')
        print()

def count_colors():
    colors = {}
    for line in breeds:
        try:
            colors[line[3]] += 1
        except KeyError:
            colors[line[3]] = 1

    lengths = [0, 0]
    for color in colors:
        if len(color) > lengths[0]:
            lengths[0] = len(color)
        if len(str(colors[color])) > lengths[1]:
            lengths[1] = len(str(colors[color]))

    print("Colors of breeds:")
    for color in colors:
        print(
                color + ' ' * (lengths[0] - len(color)) + ' ' +
                str(colors[color]) + ' ' * (lengths[1] - len(str(colors[color])))
            )

print_breeds()
print()
count_colors()

quit()

