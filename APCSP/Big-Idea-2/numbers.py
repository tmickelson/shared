# Xander

# put in number, then convert to binary
def number_example():
  print("Type 'q' to stop.")
  val = input("Type a positive number to turn into binary: ")
  while val != 'q':
    if val.isnumeric() == False:
      print("That is not a positive number ")
      val = input("Type another positive number to turn into binary:\n")
    else:
      print(bin(int(val))[2:])
      val = input("Type another positive number to turn into binary:\n")

# start()