# Toby
import blessed


term = blessed.Terminal()


def color_example():

  print("""
  Computers store colors as three or four integers. They are as follows:
  Red
  Green
  Blue
  And sometimes a transparency value
  """)
  
  while True:
    rgb_str = input(
    "Enter three values seperated by spaces: red, green, and blue. Values should be between 0 and 255:\n"
    )
    rgb_arr = rgb_str.split()

    try:
      for i in range(3):
        rgb_arr[i] = int(rgb_arr[i])
      break

    except ValueError:
      print("Please enter integers\n")

    except IndexError:
      print("Please enter three integers\n")

  print('\n' + term.on_color_rgb(rgb_arr[0], rgb_arr[1], rgb_arr[2]) +
        term.color_rgb(255, 255, 255) + "bottom text" + '\n' +
        term.on_color_rgb(rgb_arr[0], rgb_arr[1], rgb_arr[2]) +
        term.color_rgb(0, 0, 0) + "top text" + term.white_on_black + "\n\n")
