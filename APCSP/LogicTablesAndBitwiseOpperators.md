# Bitshifting

## What is bitshifting

Bitshift is the act of moving bits left or right. 

## How to use it

The bitshifting operator is "<<" for bitshift left and ">>" for bitshift right. 
The data before the operator are the bit that will be shifted. The number following the operator specifies how far to shift the bits. When new bits are created, they are 0s.

## Examples

### 1 << 1 

First convert to binary
> `0001`

Then shift left 1
> `0010`

Finally convert back to decimal
> 2

### 1 << 3 

First convert to binary
> `0001`

Then shift left 3
> `1000`

Finally convert back to decimal
> 8

### 14 >> 1 

First convert to binary
> `1110`

Then shift right 1
> `0111`

Finally convert back to decimal
> 7

