def to_base64(file):
    """
    >>> to_base64('tests/test1')
    'YWJj'
    >>> to_base64('tests/test2')
    'MTIz'
    >>> to_base64('tests/test3')
    'SXQncyBhbGwganVzdCBiaXRzIQ=='
    >>> to_base64('tests/test4')
    'SGVsbG8sIFdvcmxkISEhISE='
    """

    file = open(file, 'rb')
    lines = file.readlines()
    file.close()
    lines = bytearray().join(lines)
    bits = []
    base64 = ""
    for byte in lines:
        for bit in range(7, -1, -1):
            bits.append((byte >> bit) & 1)

    bits = bits if len(bits) % 6 == 0 else bits + ['0'] * (6 - len(bits) % 6)

    while len(bits) > 0:
        sixBits = 0
        for counter in range(5, -1, -1):
            sixBits += int(bits[0]) * (2 ** counter)
            bits.pop(0)

        base64 += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[sixBits]

    base64 += '' if len(base64) % 4 == 0 else '=' * (4 - len(base64) % 4)

    return base64
